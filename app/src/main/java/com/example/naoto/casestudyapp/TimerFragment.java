package com.example.naoto.casestudyapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class TimerFragment extends Fragment {

    private OnFragmentTimerListener mListener;

    public TimerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timer, container, false);

        Button startButton = view.findViewById(R.id.start_button);
        Button stopButton = view.findViewById(R.id.stop_button);
        Button resetButton = view.findViewById(R.id.reset_button);
//        Button saveButton = view.findViewById(R.id.save_button);
        final TextView timerText = view.findViewById(R.id.timer);

        mListener.resetTimer(timerText);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){mListener.startTimer(timerText);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                mListener.stopTimer();
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.resetTimer(timerText);
            }
        });

//        saveButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mListener.recordTime();
//            }
//        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentTimerListener) {
            mListener = (OnFragmentTimerListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentTimerListener {
        void startTimer(TextView textView);
        void stopTimer();
        void resetTimer(TextView textView);
    }
}
