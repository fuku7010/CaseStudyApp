package com.example.naoto.casestudyapp;

/* import android.app.Presentation;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

  @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
  }}



import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(this);

    }

    //ボタンが押された時の処理
    public void onClick(View view){
        Intent intent = new Intent(this, SampleActivity.class);  //インテントの作成
        startActivity(intent);                                 //画面遷移
    }
}*/
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //ボタンが押された時の処理

    public void onClick(View view) {
    }

    public void onClick_presentation(View view) {
        Intent intent = new Intent(this, PresentationActivity.class);  //インテントの作成
        startActivity(intent);                                 //画面遷移
    }


    public void onClick_submission(View view) {
        Intent intent = new Intent(this, SubmissionActivity.class);  //インテントの作成
        startActivity(intent);                                 //画面遷移
    }

}



