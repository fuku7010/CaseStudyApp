package com.example.naoto.casestudyapp;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;
import android.widget.TextView;

import org.xmlpull.v1.XmlSerializer;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class PresentationActivity extends AppCompatActivity implements TimerFragment.OnFragmentTimerListener,TimetableFragment.OnFragmentTimetableListener {

    private TextView timerText;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss.SS",Locale.US);
    private int count, period;
    private final int DEFAULT_COUNT = 15 * 60 * 100;
    private final int DEFAULT_PERIOD = 10;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            count--;
            if(count < 0) {
                count = 0;
            }
            timerText.setText(dateFormat.format(count * period));
            handler.postDelayed(this,period);
        }
    };

    @Override
    public void startTimer(TextView textView){
        timerText = textView;
        handler.removeCallbacks(runnable);
        handler.post(runnable);
    }

    @Override
    public void stopTimer(){
        handler.removeCallbacks(runnable);
    }

    @Override
    public void resetTimer(TextView textView) {
        count = DEFAULT_COUNT;
        handler.removeCallbacks(runnable);
        textView.setText(dateFormat.format(count * period));
    }

    @Override
    public void recordTime(TextView textView){
        int time = DEFAULT_COUNT - count;
        textView.setText(dateFormat.format(time * period));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentation);

        count = DEFAULT_COUNT;
        period = DEFAULT_PERIOD;

        String filename = "data.xml";
        FileOutputStream outputStream;
        XmlSerializer xmlSerializer = Xml.newSerializer();

        try{
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            xmlSerializer.setOutput(outputStream,"UTF-8");

            xmlSerializer.startDocument("UTF-8",true    );
            xmlSerializer.startTag("","Timetable");
                xmlSerializer.startTag("","Row");
                xmlSerializer.attribute("","startTime","12:00");
                xmlSerializer.attribute("","endTime","12:15");
                xmlSerializer.text("五島慶太");
                xmlSerializer.endTag("","Row");

                xmlSerializer.startTag("","Row");
                xmlSerializer.attribute("","startTime","12:15");
                xmlSerializer.attribute("","endTime","12:30");
                xmlSerializer.text("都市大太郎");
                xmlSerializer.endTag("","Row");

                xmlSerializer.startTag("","Row");
                xmlSerializer.attribute("","startTime","12:30");
                xmlSerializer.attribute("","endTime","12:45");
                xmlSerializer.text("都市大花子");
                xmlSerializer.endTag("","Row");

                xmlSerializer.startTag("","Row");
                xmlSerializer.attribute("","startTime","12:45");
                xmlSerializer.attribute("","endTime","13:00");
                xmlSerializer.text("都市大次郎");
                xmlSerializer.endTag("","Row");

                xmlSerializer.startTag("","Row");
                xmlSerializer.attribute("","startTime","13:00");
                xmlSerializer.attribute("","endTime","12:15");
                xmlSerializer.text("都市大三郎");
                xmlSerializer.endTag("","Row");

                xmlSerializer.startTag("","Row");
                xmlSerializer.attribute("","startTime","13:15");
                xmlSerializer.attribute("","endTime","13:30");
                xmlSerializer.text("都市大四郎");
                xmlSerializer.endTag("","Row");

                xmlSerializer.startTag("","Row");
                xmlSerializer.attribute("","startTime","13:30");
                xmlSerializer.attribute("","endTime","13:45");
                xmlSerializer.text("都市大五郎");
                xmlSerializer.endTag("","Row");
            xmlSerializer.endTag("","Timetable");
            xmlSerializer.endDocument();

            xmlSerializer.flush();
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        TimerFragment timerFragment = new TimerFragment();
        TimetableFragment timetableFragment = new TimetableFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container,timerFragment);
        transaction.add(R.id.container,timetableFragment);
        transaction.commit();

    }

}
